import 'package:bmi_cal/variables/const.dart';
import 'package:bmi_cal/widgets/card_info.dart';
import 'package:bmi_cal/widgets/input_cart.dart';
import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  ResultPage(
      {required this.result, required this.bmi, required this.intensive});

  final bmi;
  final result;
  final intensive;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                'Result Page',
                style: kTitleStyle,
              ),
            ),
            Expanded(
              child: InputCart(
                color: kactiveCartColor,
                cardChild: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(result, style: kGreenLable),
                    Text(
                      bmi,
                      style: kNumberStyle,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Text(
                        intensive,
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
            ),
            NavigateButton(
              label: 'RETURN',
              onPress: () => Navigator.pop(context),
            )
          ],
        ),
      ),
    );
  }
}
