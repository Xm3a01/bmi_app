import 'package:flutter/material.dart';

// varibales
const kactiveCartColor = Color(0xFF1D1E33);
const kinactiveCartColor = Color(0xFF111328);
const kbuttomContainerColor = Color(0xFFEB1555);
const kbuttomContainerHight = 80.0;

// styles
const klabelTextStyle = TextStyle(color: Color(0xFF8D8E98) , fontSize: 18);
const kNumberStyle = TextStyle(fontWeight: FontWeight.w900 , fontSize: 50);
const kButtonStyle = TextStyle(fontSize: 30,fontWeight: FontWeight.w400,);
const kGreenLable = TextStyle(color: Colors.green , fontSize: 20);
const kTitleStyle = TextStyle(fontSize: 40 , fontWeight: FontWeight.w100);

// data structure
enum Gender { male, female, no }