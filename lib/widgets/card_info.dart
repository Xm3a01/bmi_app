
import 'package:bmi_cal/variables/const.dart';
import 'package:flutter/material.dart';

class CardInfo extends StatelessWidget {
  CardInfo({required this.icon, required this.label});
  final icon;
  final label;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, size: 80),
        SizedBox(height: 10),
        Text(
          label,
          style: klabelTextStyle,
        ),
      ],
    );
  }
}

class FloatingIconButton extends StatelessWidget {
  FloatingIconButton({required this.icon, this.action});

  final icon;
  final action;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Icon(
        icon,
        size: 30,
        color: Colors.white,
      ),
      fillColor: Color(0xFF4C4F5E),
      shape: CircleBorder(),
      constraints: BoxConstraints.tightFor(height: 56, width: 56),
      elevation: 20,
      onPressed: action,
    );
  }
}

class NavigateButton extends StatelessWidget {
  NavigateButton({this.label , required this.onPress});
  final label;
  final  onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:onPress,
      child: Container(
        child: Center(
          child: Text(label, style: kButtonStyle),
        ),
        margin: EdgeInsets.only(top: 10),
        color: kbuttomContainerColor,
        height: kbuttomContainerHight,
        width: double.infinity,
      ),
    );
  }
}
