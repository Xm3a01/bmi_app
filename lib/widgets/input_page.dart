import 'package:bmi_cal/components/calculation_brin.dart';
import 'package:bmi_cal/screens/result_page.dart';
import 'package:bmi_cal/widgets/card_info.dart';
import 'package:bmi_cal/widgets/input_cart.dart';
import 'package:flutter/material.dart';
import 'package:bmi_cal/variables/const.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender selectedGender = Gender.no;
  int height = 180;
  int age = 10;
  int weihget = 100;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: InputCart(
                  onPress: () {
                    setState(() {
                      selectedGender = Gender.male;
                    });
                  },
                  color: selectedGender == Gender.male
                      ? kactiveCartColor
                      : kinactiveCartColor,
                  cardChild: CardInfo(
                    icon: Icons.male,
                    label: 'MALE',
                  ),
                ),
              ),
              Expanded(
                child: InputCart(
                  onPress: () {
                    setState(() {
                      selectedGender = Gender.female;
                    });
                  },
                  color: selectedGender == Gender.female
                      ? kactiveCartColor
                      : kinactiveCartColor,
                  cardChild: CardInfo(
                    icon: Icons.female,
                    label: 'FEMALE',
                  ),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: InputCart(
            color: kactiveCartColor,
            cardChild: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'HEIHT',
                  style: klabelTextStyle,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      '$height',
                      style: kNumberStyle,
                    ),
                    Text(
                      'cm',
                      style: klabelTextStyle,
                    ),
                  ],
                ),
                Slider(
                  value: height.toDouble(),
                  min: 120,
                  max: 220,
                  onChanged: (double newValue) {
                    setState(() {
                      height = newValue.round();
                    });
                  },
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: InputCart(
                  color: kactiveCartColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'WEIHGET',
                        style: klabelTextStyle,
                      ),
                      Text(
                        '$weihget',
                        style: kNumberStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FloatingIconButton(
                            icon: Icons.minimize,
                            action: () {
                              setState(() {
                                weihget -= 1;
                              });
                            },
                          ),
                          FloatingIconButton(
                            icon: Icons.add,
                            action: () {
                              setState(() {
                                weihget += 1;
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: InputCart(
                  color: kactiveCartColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'AGE',
                        style: klabelTextStyle,
                      ),
                      Text(
                        '$age',
                        style: kNumberStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          FloatingIconButton(
                            icon: Icons.minimize,
                            action: () {
                              setState(() {
                                age -= 1;
                              });
                            },
                          ),
                          FloatingIconButton(
                            icon: Icons.add,
                            action: () {
                              setState(() {
                                age += 1;
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        NavigateButton(
          label: 'CALCULATION',
          onPress: () {
            var calculateBmi =
                CalculationBrin(height: height, weihget: weihget);
                // print(calculateBmi.height);
                // print(calculateBmi.weihget);

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ResultPage(
                  bmi: calculateBmi.calculateBmi(),
                  result: calculateBmi.getResult(),
                  intensive: calculateBmi.getIntensive(),
                ),
              ),
            );
          },
        )
      ],
    );
  }
}
